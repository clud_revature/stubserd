# StubsERD

Full-Stack Application for managing employee company reimbursement requests!

## Application Use

Employees can:
* Register an account.
* Create request tickets.
* View their pending request tickets.
* View their past request tickets.

Managers can:
* Approve or deny requests.
* View all pending request tickets.
* View tickets by employee.

## Technologies

* HTML, CSS, JavaScript ("Web App").
* Java Servlet technology as the main engine for the application.
* Deployed to Apache Tomcat.
