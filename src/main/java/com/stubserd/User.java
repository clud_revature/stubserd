package com.stubserd;

public class User {
	
	public String first;
	public String last;
	public String email;
	public String password;
	
	public User() {
		
	}
	
	public User(String first, String last, String email, String password) {
		this.first = first;
		this.last = last;
		this.email = email;
		this.password = password;
	}
	
	public User(String first, String last, String email) {
		this.first = first;
		this.last = last;
		this.email = email;
	}
	
	public User(String email) {
		this.email = email;
	}
	
}
