package com.stubserd;

import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;

public class TestDB {
	
	public static void main(String[] args) {
		System.out.println("hello");
				
		TicketDAOPostgres dao = new TicketDAOPostgres();
		
		List<Ticket> list = dao.allTickets();
		
		for (Ticket tick : list) {
			System.out.println(tick.id);
			System.out.println(tick.description);
		}
		
		ObjectMapper om = new ObjectMapper();
		
		String listToJSON = null;
		
		try {
			listToJSON = om.writeValueAsString(list);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println(listToJSON);
	}

}
