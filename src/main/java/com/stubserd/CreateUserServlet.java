package com.stubserd;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Servlet implementation class CreateUser
 */
public class CreateUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreateUserServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		System.out.println("MADE IT TO CREATE USER SERVLET");
		
		UserDAOPostgres db = new UserDAOPostgres();
		
		String jsonInfo = request.getReader().readLine();
		
	    System.out.println(jsonInfo);
	    
	    ObjectMapper om = new ObjectMapper();
	    User us = om.readValue(jsonInfo, User.class);
	    
	    db.createUser(us);
	    
	    response.sendRedirect("CreateUserSuccess.html");	    
	    
	}

}
