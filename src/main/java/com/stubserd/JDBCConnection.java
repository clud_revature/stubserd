package com.stubserd;

import java.sql.Connection;	
import java.sql.DriverManager;
import java.sql.SQLException;

public class JDBCConnection {
	
	final String user = "postgres";
	final String password = "admin";
	final String db_url = "jdbc:postgresql:postgres";
	
	private static JDBCConnection single_instance = null;
	public Connection conn;
	
	
	public JDBCConnection() {
		
		try {
			
			Class.forName("org.postgresql.Driver");
			this.conn = DriverManager.getConnection(db_url, user, password);
			System.out.println("--> Connection to Postgres established.");

		} catch (ClassNotFoundException e) {
			System.out.println("ClassNotFound Exception..."+e);
			e.printStackTrace();
		} catch (SQLException e) {
			System.out.println("SQL Exception..."+e);
			e.printStackTrace();
		}		
	}
	
	public static JDBCConnection getInstance() {
		if (single_instance==null) {
			single_instance = new JDBCConnection();
		} 
		return single_instance;
	}

}
