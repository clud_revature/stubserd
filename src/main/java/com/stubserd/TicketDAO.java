package com.stubserd;

import java.util.List;

public interface TicketDAO {
	
	List<Ticket> allTickets();
	
	List<Ticket> allPendingTickets();
	
	List<Ticket> allEmployeeTickets(Integer users_id);
	
	Integer findAuthorId(String email);
	
	List<Ticket> allEmployeePendingTickets(Integer users_id);
	
	void submitTicket(Ticket ticket);
	
	void alterTicket(TicketApproveDeny ticket);

}
