package com.stubserd;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.postgresql.*;

public class TicketDAOPostgres implements TicketDAO {
	
	private Connection conn = JDBCConnection.getInstance().conn;

	public List<Ticket> allTickets() {
		
		List<Ticket> list = new ArrayList<Ticket>();
		
		String sql = "select * from reimbursement order by reimb_id;";
		
		System.out.println("1");
				
		System.out.println("2");

		
		try {
			System.out.println("3");

			PreparedStatement ps = conn.prepareStatement(sql);
			System.out.println("4");

			System.out.println("5");

			
			ResultSet rs = ps.executeQuery();
			System.out.println("6");

			
			while(rs.next()) {
				Double amount = rs.getDouble(1);
				Date date = rs.getDate(2);
				String description = rs.getString(3);
				Integer author = rs.getInt(5);
				Integer resolver = rs.getInt(6);
				Integer status = rs.getInt(7);
				Integer type = rs.getInt(8);
				Integer id = rs.getInt(9);
				System.out.println("ID from DB:"+ id);
				Ticket tick = new Ticket(id, amount, date, description, author, resolver, status, type);
				System.out.println("Ticket in DB: ID: "+ tick.id);
				list.add(tick);
				System.out.println("7");
			}
		} catch (Exception e) {
			System.out.println("Exception in TicketDAOPostgres:allTickets");
			e.printStackTrace();
		}
		
		return list;
	}

	public List<Ticket> allPendingTickets() {
		
		List<Ticket> list = new ArrayList<Ticket>();
		
		String sql = "select * from reimbursement where reimb_status_id = 1 order by reimb_id;";
		
		System.out.println("1");
				
		System.out.println("2");

				
		try {
			System.out.println("3");

			PreparedStatement ps = conn.prepareStatement(sql);
			System.out.println("4");

			System.out.println("5");

			
			ResultSet rs = ps.executeQuery();
			System.out.println("6");

			
			while(rs.next()) {
				Double amount = rs.getDouble(1);
				Date date = rs.getDate(2);
				String description = rs.getString(3);
				Integer author = rs.getInt(5);
				Integer resolver = rs.getInt(6);
				Integer status = rs.getInt(7);
				Integer type = rs.getInt(8);
				Integer id = rs.getInt(9);
				Ticket tick = new Ticket(id, amount, date, description, author, resolver, status, type);
				list.add(tick);
				System.out.println("7");
			}
		} catch (Exception e) {
			System.out.println("Exception in TicketDAOPostgres:allTickets");
			e.printStackTrace();
		}
		
		return list;
	}

	public List<Ticket> allEmployeeTickets(Integer users_id) {
		
		List<Ticket> list = new ArrayList<Ticket>();
		
		String sql = "select * from reimbursement where reimb_author = ? order by reimb_id;";
		
		System.out.println("1");
				
		System.out.println("2");

				
		try {
			System.out.println("3");

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, users_id);
			
			System.out.println("4");

			System.out.println("5");

			
			ResultSet rs = ps.executeQuery();
			System.out.println("6");

			
			while(rs.next()) {
				Double amount = rs.getDouble(1);
				Date date = rs.getDate(2);
				String description = rs.getString(3);
				Integer author = rs.getInt(5);
				Integer resolver = rs.getInt(6);
				Integer status = rs.getInt(7);
				Integer type = rs.getInt(8);
				Integer id = rs.getInt(9);
				Ticket tick = new Ticket(id, amount, date, description, author, resolver, status, type);
				
				list.add(tick);
				System.out.println("7");
			}
		} catch (Exception e) {
			System.out.println("Exception in TicketDAOPostgres:allEmployeeTickets");
			e.printStackTrace();
		}
		
		return list;
	}

	public Integer findAuthorId(String email) {
				
		String sql = "select users_id from users where email = ?;";
		
		Integer id = null;

				
		try {

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, email);
			
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				id = rs.getInt(1);
			}
		} catch (Exception e) {
			System.out.println("Exception in TicketDAOPostgres:findAuthorId");
			e.printStackTrace();
		}
		
		return id;
		
	}

	public List<Ticket> allEmployeePendingTickets(Integer users_id) {
		
		List<Ticket> list = new ArrayList<Ticket>();
		
		String sql = "select * from reimbursement where reimb_author = ? and reimb_status_id = 1 order by reimb_id;";
		
		System.out.println("1");
				
		System.out.println("2");

				
		try {
			System.out.println("3");

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, users_id);
			
			System.out.println("4");

			System.out.println("5");

			
			ResultSet rs = ps.executeQuery();
			System.out.println("6");

			
			while(rs.next()) {
				Double amount = rs.getDouble(1);
				Date date = rs.getDate(2);
				String description = rs.getString(3);
				Integer author = rs.getInt(5);
				Integer resolver = rs.getInt(6);
				Integer status = rs.getInt(7);
				Integer type = rs.getInt(8);
				Integer id = rs.getInt(9);
				Ticket tick = new Ticket(id, amount, date, description, author, resolver, status, type);
				list.add(tick);
				System.out.println("7");
			}
		} catch (Exception e) {
			System.out.println("Exception in TicketDAOPostgres:allEmployeePendingTickets");
			e.printStackTrace();
		}
		
		return list;
	}

	public void submitTicket(Ticket ticket) {
		
		String sql = "INSERT INTO public.reimbursement (reimb_amount,reimb_submitted,reimb_description,reimb_author,reimb_status_id,reimb_type_id) VALUES (?,?,?,?,1,?)";
		
		PreparedStatement ps;
		try {
			ps = conn.prepareStatement(sql);
			ps.setDouble(1, ticket.amount);
			ps.setDate(2, ticket.date);
			ps.setString(3, ticket.description);
			ps.setInt(4, ticket.author);
			ps.setInt(5, ticket.type_id);
			
			ps.execute();
			
		} catch (SQLException e) {
			System.out.println("Error in TicketDAOPostgres:submitTicket.");
			e.printStackTrace();
		} 
		
	}

	public void alterTicket(TicketApproveDeny ticket) {
		
		String sql = "UPDATE public.reimbursement SET reimb_status_id=? WHERE reimb_id=?;";
		
		PreparedStatement ps;
		try {
			System.out.println("Here in alterTicket");
			ps = conn.prepareStatement(sql);
			ps.setInt(1, ticket.action);
			ps.setInt(2, ticket.ticket_id);
			
			ps.execute();
			
		} catch (SQLException e) {
			System.out.println("Error in TicketDAOPostgres:submitTicket.");
			e.printStackTrace();
		} 
	}

}
