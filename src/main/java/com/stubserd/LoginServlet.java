package com.stubserd;

import java.io.IOException;  	
import java.io.PrintWriter;  
  
import javax.servlet.RequestDispatcher;  
import javax.servlet.ServletException;  
import javax.servlet.http.HttpServlet;  
import javax.servlet.http.HttpServletRequest;  
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.fasterxml.jackson.databind.ObjectMapper;



public class LoginServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public void doPost(HttpServletRequest request, HttpServletResponse response)  
	        throws ServletException, IOException {  
	  
	    //response.setContentType("text/html");  
	    //PrintWriter out = response.getWriter();
		
		
		// set session
		HttpSession session = request.getSession();  
		
		// print out session variables
		System.out.println("Session: " + session.getId());
		System.out.println("Session: " + session.getCreationTime());
		System.out.println("Session: " + session.getLastAccessedTime());
		System.out.println("Session: " + session.getMaxInactiveInterval());
	    
	    String jsonInfo = request.getReader().readLine();
	    System.out.println(jsonInfo);
	    
	    // pass in json to usersession object
	    ObjectMapper om = new ObjectMapper();
	    UserSession us = om.readValue(jsonInfo, UserSession.class);
	    
	    System.out.println(us.email);
	    System.out.println(us.password);
	    
	    UserDAOPostgres db = new UserDAOPostgres();
	    
	    //response.sendRedirect("Homepage.html");
	    
	    if (db.verifyUser(us)) {
	    	session.setAttribute("user", us.email);
	    	if (db.userRole(us) == 1) {
	    		session.setAttribute("role", 1);
	    		response.sendRedirect("ManagementHomepage.html");
	    	} else {
	    		session.setAttribute("role", 2);
	    		response.sendRedirect("Homepage.html");
	    	}
	    	
		} else {
			System.out.println("NOT AUTHENTICATED");
		}
	    
	    
	    //response.sendRedirect("Homepage");
	    
	    
	    
	          
	    //String n=request.getParameter("email");  
	    //String p=request.getParameter("password");
	    
	    //System.out.println(n + p);
	    	    
	    //RequestDispatcher rd=request.getRequestDispatcher("Homepage"); 
	    //rd.forward(request, response);
	          
//	    if(LoginDao.validate(n, p)){  
//	        RequestDispatcher rd=request.getRequestDispatcher("servlet2");  
//	        rd.forward(request,response);  
//	    }  
//	    else{  
//	        out.print("Sorry username or password error");  
//	        RequestDispatcher rd=request.getRequestDispatcher("index.html");  
//	        rd.include(request,response);  
//	    }  
	          
	    //out.close();  
	    }  
	}  


