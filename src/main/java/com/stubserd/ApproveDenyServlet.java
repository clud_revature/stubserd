package com.stubserd;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Servlet implementation class ApproveDenyServlet
 */
public class ApproveDenyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ApproveDenyServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("MADE IT TO APPROVE DENY SERVLET");
		
		String jsonInfo = request.getReader().readLine();
		
	    System.out.println(jsonInfo);
	    
	    ObjectMapper om = new ObjectMapper();
	    TicketApproveDeny tad = om.readValue(jsonInfo, TicketApproveDeny.class);
	    
	    TicketDAOPostgres db = new TicketDAOPostgres();
	    
	    db.alterTicket(tad);
	}

}
