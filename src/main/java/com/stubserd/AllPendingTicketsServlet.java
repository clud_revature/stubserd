package com.stubserd;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Servlet implementation class AllPendingTicketsServlet
 */
public class AllPendingTicketsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AllPendingTicketsServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		TicketDAOPostgres db = new TicketDAOPostgres();
		
		List<Ticket> list = db.allPendingTickets();
		
		for (Ticket tick : list) {
			System.out.println(tick.id);
			System.out.println(tick.description);
		}
		
		ObjectMapper om = new ObjectMapper();
		
		String listToJSON = null;
		
		try {
			listToJSON = om.writeValueAsString(list);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println(listToJSON);
		
		response.setContentType("application/json");
		response.getWriter().write(listToJSON);
	}

}
