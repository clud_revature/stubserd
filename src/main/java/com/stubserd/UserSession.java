package com.stubserd;

public class UserSession {
	
	public String email;
	public String password;
	
	public UserSession() {
		
	}
	
	public UserSession(String email, String password) {
		this.email = email;
		this.password = password;
	}
	
}
