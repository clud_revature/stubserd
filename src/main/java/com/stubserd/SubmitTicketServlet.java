package com.stubserd;

import java.io.IOException;
import java.sql.Date;
import java.sql.Timestamp;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Servlet implementation class SubmitTicketServlet
 */
public class SubmitTicketServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SubmitTicketServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("Here in Submit Ticket Servlet...");
		String jsonInfo = request.getReader().readLine();
		System.out.println(jsonInfo);
		
		ObjectMapper om = new ObjectMapper();
	    EmployeeTicket clientTicket = om.readValue(jsonInfo, EmployeeTicket.class);
	    System.out.println(clientTicket.amount);
	    System.out.println(clientTicket.category);
	    System.out.println(clientTicket.description);
	    
	    // get user id
	    HttpSession session = request.getSession();
	    
	    session.getAttribute("user");
	    
	    String email = (String) session.getAttribute("user");
		
		TicketDAOPostgres db_tick = new TicketDAOPostgres();
		
		Integer users_id = db_tick.findAuthorId(email);
		
		System.out.println("Users ID: "+users_id);
		
		
		// get type_id
		
		Integer type_id = 0;
		
		if (clientTicket.category.contentEquals("LODGING")) {
			type_id = 1;
		} else if (clientTicket.category.contentEquals("TRAVEL")) {
			type_id = 2;
		} else if (clientTicket.category.contentEquals("FOOD")) {
			type_id = 3;
		} else {
			type_id = 4;
		}
		
		System.out.println("Type ID: "+ type_id);
		
		
	    // (Double amount, Date date, String description, Integer author, Integer status, Integer type_id)
	    Timestamp sqlTimestamp = new java.sql.Timestamp(System.currentTimeMillis());
	    Date sqlDate = new java.sql.Date(System.currentTimeMillis());
	    
	    
	    System.out.println("SQL DATE"+ sqlDate);
	    System.out.println("TIMESTAMP:" + sqlTimestamp);
	    
	    
	    //System.out.println(Double.parseDouble(clientTicket.amount), clientTicket.description, sqlDate, 2, 1, 1);
	    Ticket dbTicket = new Ticket(Double.parseDouble(clientTicket.amount), sqlDate, clientTicket.description, users_id, 1, type_id);
	    System.out.println(dbTicket.date);
	    
	    
	    //
	    System.out.println("HERE WE GOO...");
	    db_tick.submitTicket(dbTicket);
	    System.out.println("LFG");
	    
	    
	    response.sendRedirect("SubmitTicketSuccess.html");
	    
	    
	    

	}

}
