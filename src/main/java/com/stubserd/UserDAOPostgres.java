package com.stubserd;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.postgresql.*;

public class UserDAOPostgres implements UserDAO {
	
	private Connection conn = JDBCConnection.getInstance().conn;

	public Boolean verifyUser(UserSession us) {
		String sql = "select * from users where email = ?;";
		System.out.println("1");
				
		System.out.println("2");

		
		try {
			System.out.println("3");

			PreparedStatement ps = conn.prepareStatement(sql);
			System.out.println("4");

			ps.setString(1, us.email);
			System.out.println("5");

			
			ResultSet rs = ps.executeQuery();
			System.out.println("6");

			
			while(rs.next()) {
				System.out.println("7");
				if (rs.getString(2).contentEquals(us.password)) {
					System.out.println("Made it... if.while.try.verifyuser");
					return true;
				}
			}
		} catch (Exception e) {
			System.out.println("Exception in UserDAOPostgres:verifyUser:sql");
			e.printStackTrace();
		}
		
		return false;
	}

	public void returnUsers() {
		
		String sql = "select * from users;";
		
		PreparedStatement ps;
		
		try {
			ps = conn.prepareStatement(sql);
			
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				System.out.println("HERE IN DB" + rs.getString(2));
			}
		} catch (Exception e) {
			System.out.println("Exception in UserDAOPostgres:returnUsers:sql");
			e.printStackTrace();
		}
	}

	public Integer userRole(UserSession us) {
		
		String sql = "select user_role_id from users where email = ?;";
		
		PreparedStatement ps;
		
		Integer role = 0;
		
		try {
			ps = conn.prepareStatement(sql);
			
			ps.setString(1, us.email);
			
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				role = rs.getInt(1);
			}
			
		} catch (Exception e) {
			System.out.println("Exception in UserDAOPostgres:userRole:sql");
			e.printStackTrace();
		}
		
		return role;
	}

	public void createUser(User us) {
		
		String sql = "INSERT INTO public.users (email , password, first_name, last_name, user_role_id) VALUES (?,?,?,?,2)";
		
		PreparedStatement ps;
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, us.email);
			ps.setString(2, us.password);
			ps.setString(3, us.first);
			ps.setString(4, us.last);
			
			ps.execute();
			
		} catch (SQLException e) {
			System.out.println("Error in TicketDAOPostgres:createUser.");
			e.printStackTrace();
		} 
		
	}

	public List<User> allEmployees() {
		
		List<User> list = new ArrayList<User>();
		
		String sql = "select * from users where user_role_id = 2 order by users_id;";
		
		System.out.println("1");
				
		System.out.println("2");

		
		try {
			System.out.println("3");

			PreparedStatement ps = conn.prepareStatement(sql);
			System.out.println("4");

			System.out.println("5");

			
			ResultSet rs = ps.executeQuery();
			System.out.println("6");

			
			while(rs.next()) {
				String email = rs.getString(1);
				String first = rs.getString(3);
				String last = rs.getString(4);
				User us = new User(first,last,email);
				list.add(us);
			}
		} catch (Exception e) {
			System.out.println("Exception in UserDAOPostgres:AllEmployees");
			e.printStackTrace();
		}
		
		return list;
	}

}