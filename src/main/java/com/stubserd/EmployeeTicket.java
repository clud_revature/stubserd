package com.stubserd;

import java.sql.Date;

public class EmployeeTicket {
	
	public String amount;
	public String category;
	public String description;
	
	public EmployeeTicket() {
		
	}
	
	public EmployeeTicket(String amount, String category, String description){
		this.amount = amount;
		this.category = category;
		this.description = description;
	}

}
