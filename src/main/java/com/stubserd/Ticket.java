package com.stubserd;

import java.sql.Date;

public class Ticket {
	
	public Integer id;
	public Double amount;
	public Date date;
	public String description;
	public Integer author;
	public Integer resolver;
	public Integer status;
	public Integer type_id;
	
	Ticket(Integer id, Double amount, Date date, String description, Integer author, Integer resolver, Integer status, Integer type_id){
		this.id = id;
		this.amount = amount;
		this.date = date;
		this.description = description;
		this.author = author;
		this.resolver = resolver;
		this.status = status;
		this.type_id = type_id;
	}
	
	Ticket(Double amount, Date date, String description, Integer author, Integer status, Integer type_id){
		this.amount = amount;
		this.date = date;
		this.description = description;
		this.author = author;
		this.status = status;
		this.type_id = type_id;
	}

}

