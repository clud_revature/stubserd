package com.stubserd;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Servlet implementation class EmployeeTicketsByEmailServlet
 */
public class EmployeeTicketsByEmailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EmployeeTicketsByEmailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		System.out.println("here in email employe....");
		
	    String jsonInfo = request.getReader().readLine();
	    
	    System.out.println(jsonInfo);
		
		TicketDAOPostgres db = new TicketDAOPostgres();
		
		ObjectMapper om = new ObjectMapper();
		
	    User us = om.readValue(jsonInfo, User.class);
	    
	    System.out.println(us.email);
		
	    Integer users_id = db.findAuthorId(us.email);
	    
	    System.out.println(users_id);

	    List<Ticket> list = db.allEmployeeTickets(users_id);
		
		for (Ticket tick : list) {
			System.out.println(tick.id);
			System.out.println(tick.description);
		}
				
		String listToJSON = null;
		
		try {
			listToJSON = om.writeValueAsString(list);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println(listToJSON);
		
		response.setContentType("application/json");
		response.getWriter().write(listToJSON);
	}

}
