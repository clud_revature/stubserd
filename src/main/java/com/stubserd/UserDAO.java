package com.stubserd;

import java.util.List;

public interface UserDAO {
	
	Boolean verifyUser(UserSession us);
	
	void returnUsers();
	
	Integer userRole(UserSession us);
	
	void createUser(User us);
	
	List<User> allEmployees();

}
