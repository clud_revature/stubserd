package com.stubserd;

public class TicketApproveDeny {
	
	public Integer ticket_id;
	public Integer action;
	
	public TicketApproveDeny() {
		
	}
	
	public TicketApproveDeny(Integer ticket_id, Integer action){
		this.ticket_id = ticket_id;
		this.action = action;
	}

}
