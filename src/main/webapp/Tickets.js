function createTicketTable(selector){
    // data output
    function output(data, selector){
        console.log(data);
        console.log(selector);

        function Headers(data, selector) {
            var columns = [];
            var header = $('<tr/>');
            
            for (var i = 0; i < data.length; i++) {
                var row = data[i];
                
                for (var k in row) {
                    if ($.inArray(k, columns) == -1) {
                        columns.push(k);
                        // Creating the header
                        header.append($('<th/>').html(k));
                    }
                }
                // me
            }
            // Appending the header to the table
            $(selector).append(header);
                return columns;
        }

        // Getting the all column names
        var cols = Headers(data, selector);  

        // Traversing the JSON data
        for (var i = 0; i < data.length; i++) {
            var row = $('<tr/>');
            var id = 0; 
            for (var colIndex = 0; colIndex < cols.length; colIndex++)
            {
                var val = data[i][cols[colIndex]];
                // If there is any key, which is matching
                // with the column name
                if (val == null) val = "";
                if (id == 0) id = val;
                row.append($('<td/>').html(val));
            }
            // me
            //row.append($('<td/>').html('<a href="TicketManage?id='+id+'">Manage</a>'));
            row.append($('<td/>').html('<button onClick="approve('+id+')">Approve</button><button onClick="deny('+id+')">Deny</button>'));
            // Adding each row to the table
            $(selector).append(row);
        }
    }

    async function postData(){
        const response = await fetch("http://localhost:8080/StubsERD/AllTicketsServlet", { 
            method: 'POST',
            headers: {
            'Content-Type': 'application/json'
            },
        }).then(response => response.json())
        .then(data => output(data, selector));
        //.then(data => console.log(data));
    }

    postData();
}

async function approve(id){
        const response = await fetch("http://localhost:8080/StubsERD/ApproveDenyServlet", { 
            method: 'POST',
            headers: {
            'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                action : 2,
                ticket_id : id
            }),
        }).then(window.location.reload());
    }

async function deny(id){
    const response = await fetch("http://localhost:8080/StubsERD/ApproveDenyServlet", { 
        method: 'POST',
        headers: {
        'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            action : 3,
            ticket_id : id
        }),
    }).then(window.location.reload());
}

async function renew(id){
    const response = await fetch("http://localhost:8080/StubsERD/ApproveDenyServlet", { 
        method: 'POST',
        headers: {
        'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            action : 1,
            ticket_id : id
        }),
    }).then(window.location.reload());
}


