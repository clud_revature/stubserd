async function loginCall(){
	
    let userInfo = {
        email: document.getElementById("email").value,
        password: document.getElementById("password").value
    }
	
    await fetch("LoginServlet", {
        method: "POST",
        headers: {
            'Content-Type' : 'application/json',
        },
        body: JSON.stringify(userInfo)
    }).then(response => {
		if (response.redirected){
			console.log("Here in redirected");
			window.location.href = response.url;
		} else {
            let error = document.getElementById("loginError");
            error.style.opacity = "100";
        }
    })
}