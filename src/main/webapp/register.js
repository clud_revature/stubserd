async function registerCall(){

    let userInfo = {
        first: document.getElementById("firstname").value,
        last: document.getElementById("lastname").value,
        email: document.getElementById("email").value,
        password: document.getElementById("password").value
    }
	
    await fetch("CreateUserServlet", {
        method: "POST",
        headers: {
            'Content-Type' : 'application/json',
        },
        body: JSON.stringify(userInfo)
    }).then(response => {
		if (response.redirected){
			console.log("Here in redirected");
			window.location.href = response.url;
		} else {
            
        }
    })
}