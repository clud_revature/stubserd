async function submitCall(){
	
    let userInfo = {
        amount: document.getElementById("amount").value,
        category: document.getElementById("category").value,
        description: document.getElementById("description").value
    }
	
    await fetch("SubmitTicketServlet", {
        method: "POST",
        headers: {
            'Content-Type' : 'application/json',
        },
        body: JSON.stringify(userInfo)
    }).then(response => {
		if (response.redirected){
            console.log("Here in response.")
			console.log("Here in redirected");
			window.location.href = response.url;
		} else {
            console.log("Here in else.")
            // let error = document.getElementById("loginError");
            // error.style.opacity = "100";
        }
    })
}